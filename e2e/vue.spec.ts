import { test, expect } from '@playwright/test';

// See here how to get started:
// https://playwright.dev/docs/intro
test('Går till sidan, testar om det går att logga in och se Heading', async ({ page }) => {
  await page.goto('/');
  
  //await page.getByLabel("Din mail").fill("test@test.com");

  const email = "test@test.com";
  const emailInput = await page.getByLabel("Din mail");
  for (const char of email) {
    await emailInput.type(char, {delay: 100});
  }
  
  const password = "Test123";
  const passwordInput = await page.getByLabel("Ditt lösenord");
  for (const char of password) {
    await passwordInput.type(char, { delay: 50 });
  }
  // await page.getByLabel("Ditt lösenord").fill("Test123");
  await page.getByRole("button", { name: "Skicka" }).click();

  await expect(
    page.getByRole("heading", { name: "Modul 1" }).first()
  ).toBeVisible();
});

